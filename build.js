const { buildProject } = require('@hmh/onecms-builder');
const fs = require('fs');
const { promisify } = require('util');
const readFile = promisify(fs.readFile);
const writeFile = promisify(fs.writeFile);

process.env.VERBOSE = true;

// Copy RCE launcher and config for 6-12 eBook skin into EPUB
async function postHook(explodedEpub, { label }) {
    const resourceDir = 'resources';
    const resourceList = [
        'index.html',
        'OPS/config.json',
        'OPS/assets/js/habitat_platform.js'
    ];
    console.log(`Copy RCE resources from ${resourceDir} to ${explodedEpub}`);

    for (const resource of resourceList) {
        const file = await readFile(`${resourceDir}/${resource}`);
        await writeFile(`${explodedEpub}/${resource}`, file);
    }
}

process.on('unhandledRejection', error => {
    console.log('Error:', error);
    process.exit(1);
});
  
(async ()=>{

    await buildProject({ postHook });
    console.info('Build succeeded!');
})();